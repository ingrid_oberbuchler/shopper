(function() {
  'use strict';

  angular
    .module('mysteryShopper').run(['$state', angular.noop]);
})();
