(function() {
  'use strict';

  angular
    .module('mysteryShopper')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController() {
    var vm = this;
    var originatorEv;
    vm.openMenu = function($mdOpenMenu, ev) {
      originatorEv = ev;
      $mdOpenMenu(ev);
    };
    vm.notificationsEnabled = true;
    vm.toggleNotifications = function() {
      vm.notificationsEnabled = !vm.notificationsEnabled;
    };


  }
})();
