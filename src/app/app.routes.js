(function() {
  'use strict';

  angular
    .module('mysteryShopper')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        abstract:true,
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })
      .state('home', {
        url: '/',
        templateUrl: 'app/view/home/home.html',
        controller: 'HomeController',
        controllerAs: 'vm'
      })
      .state('visit', {
        url: '/visit/:id',
        templateUrl: 'app/view/visit/visit.html',
        controller: 'VisitController',
        controllerAs: 'vm'
      });


    $urlRouterProvider.otherwise('/');
  }

})();
