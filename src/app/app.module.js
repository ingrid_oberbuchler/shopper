(function() {
  'use strict';

  angular
    .module('mysteryShopper', ['adminr-md-login',
      'ngAnimate',
      'ngMessages',
      'ngAria',
      'ngResource',
      'ui.router',
      'ngMaterial',
      'toastr',
      'md.data.table']);

})();
