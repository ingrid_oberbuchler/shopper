(function() {
  'use strict';

  angular
    .module('mysteryShopper')
    .controller('HomeController', HomeController);


  /** @ngInject */
  function HomeController(toastr, AdminrDataSources) {
    var vm = this;

    var datasource = AdminrDataSources.getDataSource();
    var Visits = datasource.getResource('Visit');
    vm.visits = Visits.query();

    activate();

    function activate() {
      vm.selected = [];

      vm.query = {
        order: 'id',
        limit: 15,
        page: 1
      };

    }
    function getVisits(query) {
      vm.promise = vm.visits.get(query, success).$promise;
    }

    function success(visits) {
      vm.visits = visits;
    }

    vm.onPaginate = function (page, limit) {
      getVisits(angular.extend({}, vm.query, {page: page, limit: limit}));
    };

    vm.onReorder = function (order) {
      getVisits(angular.extend({}, vm.query, {order: order}));
    };

  }
})();
