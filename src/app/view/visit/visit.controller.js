(function() {
  'use strict';

  angular
    .module('mysteryShopper')
    .controller('VisitController', VisitController);


  /** @ngInject */
  function VisitController($state,$stateParams,AdminrDataSources) {
    var vm = this;

    vm.id = $stateParams.id;

    var datasource = AdminrDataSources.getDataSource();
    var Visits = datasource.getResource('Visit');

    vm.visit = Visits.get({id:vm.id}, function() {

    });



  }
})();
