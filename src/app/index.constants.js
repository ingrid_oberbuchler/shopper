/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('mysteryShopper')
    .constant('moment', moment);

})();
