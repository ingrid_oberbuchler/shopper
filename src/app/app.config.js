(function() {
  'use strict';

  angular
    .module('mysteryShopper')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, AdminrMdLoginProvider,AdminrDataSourcesProvider, $mdThemingProvider) {

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-bottom-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;

    // adminr config
    AdminrMdLoginProvider.setAsRootContainerView();
    AdminrMdLoginProvider.usernameType = AdminrMdLoginProvider.USERNAME_TYPE_EMAIL;
    AdminrMdLoginProvider.setLoggedView('/app/main/main.html');

    var dataSource = AdminrDataSourcesProvider.createDataSource('ShopperAPI','https://retail-services-staging.herokuapp.com/shopper/v1');

    dataSource.addResource('Visit','/me/visits/:id',{id:'@id'});

    // MD custom theme
    var shopperGreen = $mdThemingProvider.extendPalette('green', {
      '500': '#43b748',
      '50': '#dfdfdf',
      'contrastDefaultColor': 'light'
    });
    $mdThemingProvider.definePalette('green', shopperGreen);
    $mdThemingProvider.theme('default')
      .primaryPalette('green')
      .accentPalette('grey', {
        'default': '300'
      });
  }

})();
